FROM node:latest
RUN mkdir -p /usr/src/nodedockermongoexample
WORKDIR /usr/src/nodedockermongoexample
COPY package.json /usr/src/nodedockermongoexample/
RUN npm install
RUN npm install -g concurrently
COPY . /usr/src/nodedockermongoexample
EXPOSE 8080
CMD ["concurrently","npm:start"]