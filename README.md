# Docker-Node-Mongo Ejemplo

Pueden encontrar el paso a paso de este proyecto en el siguiente Link: https://bit.ly/3359OAL

## Servicios

### Listar usuario
http://localhost:4000/api/usuario/
```

[
    {
        "_id": "5f6f5f9363fc133d30dd432d",
        "nombre": "julian",
        "email": "julian@gmail.com",
        "telefono": 44444544,
        "__v": 0
    }
]
```

### Crear usuario
```

curl --location --request POST 'http://localhost:4000/api/usuario/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "nombre":"julianusuario2",
    "email":"julian2@gmail.com",
    "telefono":44444544
}'
```

### Listar 1 usuario
```

http://localhost:4000/api/usuario/{{ID_USUARIO}

{
    "nombre":"julianusuario2",
    "email":"julian2@gmail.com",
    "telefono":44444544
}
```

### Actualizar usuario
```

curl --location --request PUT 'http://localhost:4000/api/usuario/{{ID_USUARIO}}' \
--header 'Content-Type: application/json' \
--data-raw '{
    "nombre":"julianusuario4",
    "email":"julian2@gmail.com",
    "telefono":111
}'
```

### Eliminar usuario
```
curl --location --request DELETE 'http://localhost:4000/api/usuario/{{ID_USUARIO}}' \
--header 'Content-Type: application/json' \
--data-raw '{
    "nombre":"julianusuario4",
    "email":"julian2@gmail.com",
    "telefono":111
}'
```
