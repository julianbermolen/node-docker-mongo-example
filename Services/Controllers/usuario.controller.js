const Usuario = require('../Models/usuario');

// instanciamos un array donde almacenaremos las respuestas de nuestras funciones.
const usuarioCtrl = {};

// Este servicio listará todos los usuarios en la BDD
// Toda solicitud tiene un request y un response. Usaremos el Response para devolverlo en forma de JSON.
// Como verán, el esquema puede utilizar la funcion find(), que en mongo debe nos traerá la seleccion de ese mismo esquema.
usuarioCtrl.getUsers = async (req,res) => {
    let usuarios = await Usuario.find();
    res.json(usuarios);
};

// Creamos un usuario. COmo verán, la información vendrá en el Request por Body. El verbo utilizado es POST como lo indica el router.
// En este caso, utilizamos la función del esquema Save, para guardar el usuario en BDD.
usuarioCtrl.createUser = async (req,res) => {
    const usuario = new Usuario({
        nombre: req.body.nombre,
        email: req.body.email,
        telefono: req.body.telefono
    });
    await usuario.save();
    res.json({
           'status':'Employee saved'
    });
};

//Obtenemos el ID por parámetro y buscamos con la funcion findById del esquema Mongoose.
// Si obtenemos el usuario, lo respondemos en forma de JSON y Si no, respondemos con error.
usuarioCtrl.getUser = async (req,res) => {
    let user = await Usuario.findById(req.params.id, function(err,usuario){
        if(err){
            res.status(404).json({'status':'Usuario no encontrado en MongoDB'});
        }
        else res.json(usuario);
    });
};

//Usamos la funcionalidad findByIdAndUpdate para buscar y actualizar con la nueva información.
usuarioCtrl.editUser = async (req,res) => {
    let user = {
        nombre: req.body.nombre,
        email: req.body.email,
        telefono: req.body.telefono
    };
    let usuario = await Usuario.findByIdAndUpdate(req.params.id, {$set: user}, {new:true});
    res.json({'status': 'Usuario actualizado',usuario});
};

//De la misma forma, buscamos por id y lo removemos con la funcionalidad findByIdAndRemove
usuarioCtrl.deleteUser = async (req,res) => {
    await Usuario.findByIdAndRemove(req.params.id);
    res.json({'Status':'Usuario eliminado'});
};

module.exports = usuarioCtrl;