const mongoose = require('mongoose');

let Schema = mongoose.Schema;

//Definimos y creamos el esquema "usuario"
let UsuarioSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es necesario'],
    },
    email: {
        type: String,
        unique: true,
        required: [true, "El correo es necesario"],
    },
    telefono: {
        type: Number,
        required: [true, "El teléfono es necesario"]
    }
});

module.exports = mongoose.model('Usuario', UsuarioSchema)