const express = require('express');
const router = express.Router();

//Requerimos el archivo controlador donde estarán nuestras funciones
const userCtrl = require('../Controllers/usuario.controller');

//Instanciamos dentro de nuestro Router, los verbos Rest necesarios.

router.get('/', userCtrl.getUsers);
router.post('/',userCtrl.createUser);
router.get('/:id', userCtrl.getUser);
router.put('/:id', userCtrl.editUser);
router.delete('/:id', userCtrl.deleteUser);

module.exports = router;