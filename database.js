//Instanciamos mongoose
const mongoose = require('mongoose');

// Agrego en una constante la dirección de la base de datos, que en caso de no existir, será creada por mongoose.
const URI = 'mongodb://localhost/usuario';

//Utilizo la función connect, enviandole la dirección de mi base de datos y colocando un metodo de control de errores
mongoose.connect(URI,{useNewUrlParser:true,useUnifiedTopology:true})
	.then(db => console.log('DB is Connected'))
	.catch(err => console.error(err));

//Luego, exportamos la conexión que será utilizada en el archivo principal del proyecto.

module.exports = mongoose;