const express = require('express')
const app = express()
const bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

// Settings

//De este archivo, solo quiero la conexion, osea mongose.
const { mongoose } = require('./database');

//Process.env.PORT es el puerto dado por el ambiente. Si no existe, será 4000 nuestro puerto.
app.set('port', process.env.PORT || 4000); 

// Middlewares 
app.use(express.json());
app.use(bodyParser.urlencoded({extended: false}));  

// Routes de la aplicación
app.use('/api/usuario',require('./Services/Routes/usuario.routes'));

// Inicia la aplicación en el puerto correspondiente.
app.listen(app.get('port'), () =>{
	console.log('Server on port', app.get('port'));

});